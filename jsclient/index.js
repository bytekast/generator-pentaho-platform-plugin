'use strict';
var util = require('util');
var path = require('path');
var yeoman = require('yeoman-generator');


var PentahoJsclientGenerator = module.exports = function PentahoJsclientGenerator(args, options, config) {
  yeoman.generators.Base.apply(this, arguments);
  this.argument('appname', { type: String, required: false });
  this.appname = this.appname || path.basename(process.cwd());
  this.appname = this._.camelize(this._.slugify(this._.humanize(this.appname)));

  this.on('end', function () {
    this.installDependencies({ skipInstall: options['skip-install'] });
  });

  this.pkg = JSON.parse(this.readFileAsString(path.join(__dirname, '../package.json')));
};

util.inherits(PentahoJsclientGenerator, yeoman.generators.Base);

PentahoJsclientGenerator.prototype.askFor = function askFor() {
  var cb = this.async();

  // have Yeoman greet the user.
  console.log(this.yeoman);

  var prompts = [];
  this.prompt(prompts, function (props) {
    this.someOption = props.someOption;

    cb();
  }.bind(this));
};

PentahoJsclientGenerator.prototype.app = function app() {

  var resourceWeb = 'package-res/resources/web/';

  this.copy('../seed/Gruntfile.js', resourceWeb + 'Gruntfile.js');
  this.copy('../seed/karma.conf.js', resourceWeb + 'karma.conf.js');
  this.copy('../seed/karma-e2e.conf.js', resourceWeb + 'karma-e2e.conf.js');
  //this.copy('../seed/package.json','package.json');

  this.mkdir(resourceWeb + 'app');
  this.copy('../seed/app/.buildignore', resourceWeb + 'app/.buildignore');
  this.copy('../seed/app/.htaccess', resourceWeb + 'app/.htaccess');
  this.copy('../seed/app/404.html', resourceWeb + 'app/404.html');
  this.copy('../seed/app/favicon.ico', resourceWeb + 'app/favicon.ico');
  this.copy('../seed/app/index.html', resourceWeb + 'app/index.html');
  this.copy('../seed/app/robots.txt', resourceWeb + 'app/robots.txt');

  //this.mkdir('app/templates');

  this.mkdir(resourceWeb + 'app/scripts');
  this.copy('../seed/app/scripts/app.js', resourceWeb + 'app/scripts/app.js');
  this.copy('../seed/app/scripts/bootstrap.js', resourceWeb + 'app/scripts/bootstrap.js');

  // copy controllers
  this.mkdir(resourceWeb + 'app/scripts/controllers');
  this.copy('../seed/app/scripts/controllers/main.js', resourceWeb + 'app/scripts/controllers/main.js');
  this.copy('../seed/app/scripts/controllers/controllers.js', resourceWeb + 'app/scripts/controllers/controllers.js');

  // copy directives
  this.mkdir(resourceWeb + 'app/scripts/directives');
  this.copy('../seed/app/scripts/directives/directives.js', resourceWeb + 'app/scripts/directives/directives.js');

  // copy filters
  this.mkdir(resourceWeb + 'app/scripts/filters');
  this.copy('../seed/app/scripts/filters/filters.js', resourceWeb + 'app/scripts/filters/filters.js');

  // copy services
  this.mkdir(resourceWeb + 'app/scripts/services');
  this.copy('../seed/app/scripts/services/services.js', resourceWeb + 'app/scripts/services/services.js');

  this.mkdir(resourceWeb + 'app/styles');
  this.copy('../seed/app/styles/bootstrap.css', resourceWeb + 'app/styles/bootstrap.css');
  this.copy('../seed/app/styles/main.css', resourceWeb + 'app/styles/main.css');

  this.mkdir(resourceWeb + 'app/views');
  this.copy('../seed/app/views/main.html', resourceWeb + 'app/views/main.html');


  /***** TEST ****/

  this.mkdir(resourceWeb + 'test');
  this.copy('../seed/test/.jshintrc', resourceWeb + 'test/.jshintrc');
  this.copy('../seed/test/runner.html', resourceWeb + 'test/runner.html');

  this.mkdir(resourceWeb + 'test/spec');
  this.copy('../seed/test/spec/main.js', resourceWeb + 'test/spec/main.js');

  this.mkdir(resourceWeb + 'test/spec/controllers');
  this.copy('../seed/test/spec/controllers/mainSpec.js', resourceWeb + 'test/spec/controllers/mainSpec.js');

  this.template('_package.json', resourceWeb + 'package.json');
  this.template('_bower.json', resourceWeb + 'bower.json');
};

PentahoJsclientGenerator.prototype.projectfiles = function projectfiles() {

  var resourceWeb = 'package-res/resources/web/';

  //this.copy('editorconfig', '.editorconfig');
  //this.copy('jshintrc', '.jshintrc');
  this.copy('bowerrc', resourceWeb + '.bowerrc');
};
