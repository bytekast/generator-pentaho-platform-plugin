'use strict';
var util = require('util');
var path = require('path');
var yeoman = require('yeoman-generator');


var PentahoPlatformPluginGenerator = module.exports = function PentahoPlatformPluginGenerator(args, options, config) {
  yeoman.generators.Base.apply(this, arguments);
  this.argument('pluginid', { type: String, required: false });
  this.pluginid = this.pluginid || path.basename(process.cwd());
  this.pluginid = this._.camelize(this._.slugify(this._.humanize(this.pluginid)));

  this.on('end', function () {
    this.installDependencies({ skipInstall: options['skip-install'] });
  });

  this.pkg = JSON.parse(this.readFileAsString(path.join(__dirname, '../package.json')));
};

util.inherits(PentahoPlatformPluginGenerator, yeoman.generators.Base);

PentahoPlatformPluginGenerator.prototype.askFor = function askFor() {
  var cb = this.async();

  console.log('Welcome to the Pentaho Platform Plugin Generator!');

  var prompts = [{
    name: 'pluginid',
    message: 'Enter plugin id: ',
    default: this.pluginid
  },
  {
    name: 'pluginname',
    message: 'Enter plugin name/title: ',
    default: 'Custom Pentaho Platform Plugin'
  },
  {
    name: 'organization',
    message: 'Enter organization or artifact group: ',
    default: 'com.pentaho'
  },
  {
    name: 'pentahoDir',
    message: 'Enter Pentaho BISERVER location: ',
    default: '/Applications/pentaho/server/biserver'
  }];

  this.prompt(prompts, function (props) {
    this.pluginid = props.pluginid;
    this.pluginname = props.pluginname;
    this.organization = props.organization;
    this.pentahoDir = props.pentahoDir;

    cb();
  }.bind(this));
};

PentahoPlatformPluginGenerator.prototype.app = function app() {

  this.template('../seed/build.properties', 'build.properties');
  this.template('../seed/build.xml', 'build.xml');
  this.copy('../seed/ivy.xml', 'ivy.xml');
  this.copy('../seed/ivysettings.xml', 'ivysettings.xml');
  this.copy('../seed/package-ivy.xml', 'package-ivy.xml');

  this.mkdir('build-res');
  this.copy('../seed/build-res/subfloor.xml', 'build-res/subfloor.xml');
  this.copy('../seed/build-res/subfloor-pkg.xml', 'build-res/subfloor-pkg.xml');
  this.copy('../seed/build-res/subfloor-pkg.xml', 'build-res/subfloor-ee.xml');

  this.mkdir('package-res');
  this.template('../seed/package-res/plugin.xml', 'package-res/plugin.xml');
  this.template('../seed/package-res/settings.xml', 'package-res/settings.xml');
  this.copy('../seed/package-res/version.xml', 'package-res/version.xml');
  this.copy('../seed/package-res/plugin.spring.xml', 'package-res/plugin.spring.xml');

  this.mkdir('package-res/resources');
  this.mkdir('package-res/resources/web');

  this.copy('../seed/package-res/resources/web/require-js-cfg.js',
    'package-res/resources/web/' + this.pluginid + '-require-js-cfg.js');

  this.mkdir('src');
  this.mkdir('lib');

  this.mkdir('test-src');
  this.mkdir('test-lib');

  //this.mkdir('app');
  //this.mkdir('app/templates');

  this.template('_package.json', 'package.json');
  //this.template('_bower.json', 'bower.json');
};

PentahoPlatformPluginGenerator.prototype.projectfiles = function projectfiles() {
  //this.copy('editorconfig', '.editorconfig');
  //this.copy('jshintrc', '.jshintrc');
  //this.copy('bowerrc', '.bowerrc');
};
