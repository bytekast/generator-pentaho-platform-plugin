// main module for all controllers
define(['angular'], function(angular) {
   'use strict';
   return angular.module('controllers', []);
});