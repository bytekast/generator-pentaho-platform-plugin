// Configure RequireJS
requirejs.config({
  baseUrl: 'scripts',
  paths: {
    'angular-cookies': '../bower_components/angular-cookies/angular-cookies',
    'angular-mocks': '../bower_components/angular-mocks/angular-mocks',
    'angular-resource': '../bower_components/angular-resource/angular-resource',
    'angular-sanitize': '../bower_components/angular-sanitize/angular-sanitize',
    'angular-scenario': '../bower_components/angular-scenario/angular-scenario',
    angular: '../bower_components/angular/angular',
    json3: '../bower_components/json3/build',
    'requirejs-domready': '../bower_components/requirejs-domready/domReady',
    'es5-shim': '../bower_components/es5-shim/es5-shim',
    requirejs: '../bower_components/requirejs/require',
    jquery: '../bower_components/jquery/jquery'
  },
  shim: {
    angular: {
      deps: [
        'jquery'
      ],
      exports: 'angular'
    }
  }
});


// Start the main app logic.
require([
  'angular',
  'requirejs-domready',
  'app'
],
  function (angular, domReady) {
    domReady(function() {
      angular.bootstrap(document, ['pentahoAngularSeedApp']);
    });
  }
);
